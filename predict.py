import cv2
import mediapipe as mp
import keras
import numpy as np
from math import sin, cos, radians, pi, atan2, degrees
import time

mp_holistic = mp.solutions.holistic
mp_drawing = mp.solutions.drawing_utils 
mp_drawing_styles = mp.solutions.drawing_styles

def euclidean_dist(a, b):
    try:
        if (a.shape[1] == 2 and a.shape == b.shape):
            bol_a = (a[:,0] != 0).astype(int)
            bol_b = (b[:,0] != 0).astype(int)
            dist = np.linalg.norm(a-b, axis=1)
            return((dist*bol_a*bol_b).reshape(a.shape[0],1))
    except:
        print("[Error]: Check dimension of input vector")
        return 0

def norm_X(X):
    num_sample = X.shape[0]
    # Keypoints
    Nose = X[:,0*2:0*2+2]
    Neck = X[:,1*2:1*2+2]
    RShoulder = X[:,2*2:2*2+2]
    RElbow = X[:,3*2:3*2+2]
    RWrist = X[:,4*2:4*2+2]
    LShoulder = X[:,5*2:5*2+2]
    LElbow = X[:,6*2:6*2+2]
    LWrist = X[:,7*2:7*2+2]
    RHip = X[:,8*2:8*2+2]
    RKnee = X[:,9*2:9*2+2]
    RAnkle = X[:,10*2:10*2+2]
    LHip = X[:,11*2:11*2+2]
    LKnee = X[:,12*2:12*2+2]
    LAnkle = X[:,13*2:13*2+2]
    REye = X[:,14*2:14*2+2]
    LEye = X[:,15*2:15*2+2]
    REar = X[:,16*2:16*2+2]
    LEar = X[:,17*2:17*2+2]

    # Length of head
    length_Neck_LEar = euclidean_dist(Neck, LEar)
    length_Neck_REar = euclidean_dist(Neck, REar)
    length_Neck_LEye = euclidean_dist(Neck, LEye)
    length_Neck_REye = euclidean_dist(Neck, REye)
    length_Nose_LEar = euclidean_dist(Nose, LEar)
    length_Nose_REar = euclidean_dist(Nose, REar)
    length_Nose_LEye = euclidean_dist(Nose, LEye)
    length_Nose_REye = euclidean_dist(Nose, REye)
    length_head      = np.maximum.reduce([length_Neck_LEar, length_Neck_REar, length_Neck_LEye, length_Neck_REye, \
                                 length_Nose_LEar, length_Nose_REar, length_Nose_LEye, length_Nose_REye])
    #length_head      = np.sqrt(np.square((LEye[:,0:1]+REye[:,0:1])/2 - Neck[:,0:1]) + np.square((LEye[:,1:2]+REye[:,1:2])/2 - Neck[:,1:2]))

    # Length of torso
    length_Neck_LHip = euclidean_dist(Neck, LHip)
    length_Neck_RHip = euclidean_dist(Neck, RHip)
    length_torso     = np.maximum(length_Neck_LHip, length_Neck_RHip)
    #length_torso     = np.sqrt(np.square(Neck[:,0:1]-(LHip[:,0:1]+RHip[:,0:1])/2) + np.square(Neck[:,1:2]-(LHip[:,1:2]+RHip[:,1:2])/2))

    # Length of right leg
    length_leg_right = euclidean_dist(RHip, RKnee) + euclidean_dist(RKnee, RAnkle)
    #length_leg_right = np.sqrt(np.square(RHip[:,0:1]-RKnee[:,0:1]) + np.square(RHip[:,1:2]-RKnee[:,1:2])) \
    #+ np.sqrt(np.square(RKnee[:,0:1]-RAnkle[:,0:1]) + np.square(RKnee[:,1:2]-RAnkle[:,1:2]))

    # Length of left leg
    length_leg_left = euclidean_dist(LHip, LKnee) + euclidean_dist(LKnee, LAnkle)
    #length_leg_left = np.sqrt(np.square(LHip[:,0:1]-LKnee[:,0:1]) + np.square(LHip[:,1:2]-LKnee[:,1:2])) \
    #+ np.sqrt(np.square(LKnee[:,0:1]-LAnkle[:,0:1]) + np.square(LKnee[:,1:2]-LAnkle[:,1:2]))

    # Length of leg
    length_leg = np.maximum(length_leg_right, length_leg_left)

    # Length of body
    length_body = length_head + length_torso + length_leg
    
    # Check all samples have length_body of 0
    length_chk = (length_body > 0).astype(int)
    
    # Check keypoints at origin
    keypoints_chk = (X > 0).astype(int)
    
    chk = length_chk * keypoints_chk
    
    # Set all length_body of 0 to 1 (to avoid division by 0)
    length_body[length_body == 0] = 1
    
    # The center of gravity
    # number of point OpenPose locates:
    num_pts = (X[:, 0::2] > 0).sum(1).reshape(num_sample,1)
    centr_x = X[:, 0::2].sum(1).reshape(num_sample,1) / num_pts
    centr_y = X[:, 1::2].sum(1).reshape(num_sample,1) / num_pts

    # The  coordinates  are  normalized relative to the length of the body and the center of gravity
    X_norm_x = (X[:, 0::2] - centr_x) / length_body
    X_norm_y = (X[:, 1::2] - centr_y) / length_body
    
    # Stack 1st element x and y together
    X_norm = np.column_stack((X_norm_x[:,:1], X_norm_y[:,:1]))
        
    for i in range(1, X.shape[1]//2):
        X_norm = np.column_stack((X_norm, X_norm_x[:,i:i+1], X_norm_y[:,i:i+1]))
    
    # Set all samples have length_body of 0 to origin (0, 0)
    X_norm = X_norm * chk
    
    return X_norm

def keypoints_prediction(video_name):
    cap = cv2.VideoCapture(video_name)
    keypoint_list =[]
    keypoints_dict=dict()
    index_1 = 0
    while cap.isOpened():
        success, image = cap.read()
        if not success:
            print("Ignoring empty camera frame.")
            break
        with mp_holistic.Holistic(
            min_detection_confidence=0.5,
            min_tracking_confidence=0.5) as holistic:
            image.flags.writeable = False
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            results = holistic.process(image)
            if hasattr(results.pose_landmarks, 'landmark'):
                keypoints = results.pose_landmarks.landmark
                keypoints_dict[index_1] = keypoints
                index_1+=1
                index= 0
                temp_dict={}
                final_points =[]
                indices= [0,33,11,13,15,12,14,16,23,25,27,24,26,28,2,5,7,8]
                for idx, kp in enumerate(keypoints):
                    temp_dict[idx] = (kp.x,kp.y)
                x2=temp_dict[12][0]
                x1=temp_dict[11][0]
                y2=temp_dict[12][1]
                y1=temp_dict[11][1]
                neck_x =(x1 + x2) / 2
                neck_y =(y1 + y2) / 2
                temp_dict[33] = (neck_x,neck_y)
                for idx in indices:
                    final_points.append(temp_dict[idx][0])
                    final_points.append(temp_dict[idx][1])
                keypoint_list.append(final_points)
    cap.release()
    return keypoint_list, keypoints_dict

def predict_pose(model, keypoints, labels, is_array=True):
    """
    model : keras model
    keypoints : array or dict of keypoints
    is_array : keypoints type 
    """
    if is_array:
        data_norm = norm_X(keypoints)
        y_out = model.predict(data_norm)
        pose = [labels[x] for x in np.argmax(y_out, axis=1)]
        return {'pose': pose, 'conf': np.max(y_out, axis=1)} 
    else:
        data = preprocess_keypoints(keypoints)
        data_norm = norm_X(data)
        y_out = model.predict(data_norm)
        return {'pose': labels[np.argmax(y_out)], 'conf': np.max(y_out)}

def load_pose_model(exercise_id):
    path_maps = {0:'models/SAA.h5',1:'models/SLROKB.h5',2:'models/SAA.h5',3:'models/SKF.h5',
    4:'models/SLKF.h5',5:'models/SLROLS.h5'}
    model_path = path_maps[exercise_id]
    model = keras.models.load_model(model_path)
    return model_path , model

# Testing on single example
def pose_prediction(model_name, model, keypoint_list):
    if model_name.split('/')[1].split('.')[0]== 'SLROKB':
        LABELS = ['relax','left_knee_extension','right_knee_extension']
    if model_name.split('/')[1].split('.')[0]== 'SAA':
        LABELS = ['relax','left_knee_extension','right_knee_extension']
    elif model_name.split('/')[1].split('.')[0]== 'SKF':
        LABELS = ['relax','knee_flex','knee_flex']
    elif model_name.split('/')[1].split('.')[0]== 'SLKF':
        LABELS = ['relax','left_knee_extension','right_knee_extension']
    elif model_name.split('/')[1].split('.')[0]== 'SLROLS':
        LABELS = ['relax','left_leg_raise','right_leg_raise']
    ordered_keypoints = ['Nose', 'Neck', 'RShoulder', 'RElbow', 'RWrist', 'LShoulder', 
                        'LElbow', 'LWrist', 'RHip', 'RKnee', 'RAnkle', 'LHip', 'LKnee', 
                        'LAnkle', 'REye', 'LEye', 'REar', 'LEar']
    keypoints = np.array(keypoint_list)
    preds = predict_pose(model, keypoints, LABELS)
    return preds

def angle_to(p1, p2, rotation=0, clockwise=False):
    angle = degrees(atan2(p2[1] - p1[1], p2[0] - p1[0])) - rotation
    if not clockwise:
        angle = -angle
    return angle % 360

def return_ht_metrics(preds):
    for idx , pose in enumerate(preds['pose']):
        listlength = len(preds['pose'])
        if idx +5 == listlength:
            break 
        if preds['pose'][idx] != preds['pose'][idx+1]:
            for i in range(2,7):
                if preds['pose'][idx+i] != preds['pose'][idx+1]:
                    preds['pose'][idx+1] = preds['pose'][idx]

    myset = set(preds['pose'])
    labels_dict = dict()
    for i,v in enumerate(myset):
        labels_dict[v] = i
    predictions = []
    for i in preds['pose']:
        predictions.append(labels_dict[i])
    relax_value = labels_dict['relax']
    transitions = [i for i in range(1,len(predictions)) if predictions[i]!=predictions[i-1] ]

    reps_count =0
    relax_indexes = []
    for trans in transitions:
        if predictions[trans] == relax_value:
            reps_count+=1
            relax_indexes.append(trans)

    start_index= 0
    hts = []
    for idx , trans in enumerate(relax_indexes):
        if (idx+1) < len(relax_indexes):
            current_ht = (relax_indexes[idx+1] - trans) / 30
            hts.append(current_ht)
    hts.insert(0,relax_indexes[0]/30)
    hts.append(0)
    return hts,reps_count

def return_angle_metrics(keypoint_list, model_name,keypoints_dict,preds):
    angle_list =[]
    for index, points in enumerate(keypoint_list):
        if model_name.split('/')[1].split('.')[0]== 'SLROLS' or model_name.split('/')[1].split('.')[0]== 'SLROKB':
            right_hip_point = (keypoints_dict[index][24].x,keypoints_dict[index][24].y)
            right_ankle_point =(keypoints_dict[index][28].x,keypoints_dict[index][28].y)
            left_hip_point = (keypoints_dict[index][23].x,keypoints_dict[index][23].y)
            left_ankle_point =(keypoints_dict[index][27].x,keypoints_dict[index][27].y)
            if preds['pose'][index].startswith('left'):
                angle = angle_to(left_hip_point, left_ankle_point , rotation=0, clockwise=False)
                angle_list.append(angle)
            else:
                angle = angle_to(right_hip_point, right_ankle_point , rotation=0, clockwise=False)
                angle_list.append(angle)
        elif model_name.split('/')[1].split('.')[0]== 'SLKF':
            right_hip_point = (keypoints_dict[index][24].x,keypoints_dict[index][24].y)
            right_ankle_point =(keypoints_dict[index][26].x,keypoints_dict[index][26].y)  
            left_hip_point = (keypoints_dict[index][23].x,keypoints_dict[index][23].y)
            left_ankle_point =(keypoints_dict[index][25].x,keypoints_dict[index][25].y)
            if preds['pose'][index].startswith('left'):
                angle = angle_to(left_hip_point, left_ankle_point , rotation=0, clockwise=False)
                angle_list.append(angle)
            else:
                angle = angle_to(right_hip_point, right_ankle_point , rotation=0, clockwise=False)
                angle_list.append(angle)
        elif model_name.split('/')[1].split('.')[0]== 'SKF':
            right_hip_point = (keypoints_dict[index][24].x,keypoints_dict[index][24].y)
            right_ankle_point =(keypoints_dict[index][28].x,keypoints_dict[index][28].y)  
            left_hip_point = (keypoints_dict[index][23].x,keypoints_dict[index][23].y)
            left_ankle_point =(keypoints_dict[index][27].x,keypoints_dict[index][27].y)  
            if preds['pose'][index].startswith('left'):
                angle = angle_to(left_hip_point, left_ankle_point , rotation=0, clockwise=True)
                angle_list.append(angle)
            else:
                angle = angle_to(right_hip_point, right_ankle_point , rotation=0, clockwise=True)
                angle_list.append(angle)
        elif model_name.split('/')[1].split('.')[0]== 'SAA':
            right_hip_point = (keypoints_dict[index][24].x,keypoints_dict[index][24].y)
            right_ankle_point =(keypoints_dict[index][28].x,keypoints_dict[index][28].y)  
            left_hip_point = (keypoints_dict[index][23].x,keypoints_dict[index][23].y)
            left_ankle_point =(keypoints_dict[index][27].x,keypoints_dict[index][27].y)
            if preds['pose'][index].startswith('left'):
                angle = angle_to(left_hip_point, left_ankle_point , rotation=0, clockwise=False)
                angle_list.append(angle)
            else:
                angle = angle_to(right_hip_point, right_ankle_point , rotation=0, clockwise=False)
                angle_list.append(angle)
    return angle_list


def get_results(video_name, model_path, model):
    result_dict = dict()
    exercise_id = 0
    keypoint_list, keypoints_dict = keypoints_prediction(video_name)
    preds = pose_prediction(model_path, model,keypoint_list)
    ht,reps = return_ht_metrics(preds)
    angle_metrics = return_angle_metrics(keypoint_list,model_path,keypoints_dict,preds)
    result_dict['angle'] = angle_metrics
    result_dict['hold_time'] = ht
    result_dict['reps'] = reps
    return result_dict


if __name__ == "__main__":
    start = time.time()
    #Here you can load the model.
    exercise_id =0
    model_path , model = load_pose_model(exercise_id)
    video_name = 'vids/shaa_3.mp4'
    res = get_results(video_name,model_path, model)
    print("RESE",res)
    end = time.time() -start 
    print(end)





